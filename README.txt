CONTENTS OF THIS FILE
---------------------
 * Description
 * Introduction
 * Requirements
 * Installing
 * Uninstalling
 * Frequently Asked Questions (FAQ)
 * Known Issues
 * More Information


DESCRIPTION
-----------
This module will help you to resize single or multiple images.
There is the option to set height, width, a directory 
where you wanted to download your images.
Also ability to upload compress file,
the module will automatically extract images and 
resize all the images as per the preference
that you have set at upload time.
Just goto {/admin/config/media/bulk_image_resizer}
where all the option already set.

INTRODUCTION
------------

Current Maintainer: Dhruv Panchal <https://www.drupal.org/u/dhruv-panchal> 

REQUIREMENTS
------------

"No special requirements"

INSTALLATION
----------

See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once Bulk Image Resizer module is installed and enabled,
you can adjust the settings for your
site's at admin/config/media/bulk_image_resizer

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
When enabled, the module will prevent the links from appearing.
To get the links back, disable the module and clear caches.

UNINSTALLING
------------

You can easily uninstall module from module list, there is no any dependency


FREQUENTLY ASKED QUESTIONS (FAQ)
--------------------------------

- There are no frequently asked questions at this time.


KNOWN ISSUES
------------

Error - Error at the place of button
Solution :  Please confirm that you placed right channel name 
or channel id in configuration page


MORE INFORMATION
----------------

- There are no more information.
