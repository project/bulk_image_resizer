<?php

/**
 * @file
 * Provides functions for Administration settings.
 */

module_load_include('php', 'bulk_image_resizer', '/class.imageresizer');
module_load_include('inc', 'bulk_image_resizer', '/system.archiver');

/**
 * Implements hook_form().
 */
function bulk_image_resizer_form($form, &$form_state) {
  $form['width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
  );
  $form['height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
  );
  $form['file'] = array(
    '#type' => 'managed_file',
    '#title' => t('Upload File'),
    '#description' => t('Upload a file, allowed extensions: jpg, jpeg, png, gif, zip'),
    '#upload_validators' => array(
      'file_validate_extensions' => array('jpg jpeg png gif zip'),
    ),
    '#upload_location' => 'public://bulk_image/img',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Resize'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function bulk_image_resizer_form_submit($form, &$form_state) {
  $width = $form_state['values']['width'];
  $height = $form_state['values']['height'];
  $file = file_load($form_state['values']['file']);
  $image_name = $file->filename;
  $uri = 'public://bulk_image/img';
  $path_img = drupal_realpath($uri);
  $dir_thumb = 'public://bulk_image/thumb';
  $path_thumb = drupal_realpath($dir_thumb);
  file_prepare_directory($dir_thumb, FILE_CREATE_DIRECTORY);
  $file_mime_zip = $file->filemime;
  if ($file_mime_zip == 'application/zip') {
    $extractor = new Extractor();
    $archivePath = $path_img . '/' . $image_name;
    $destPath = $path_img;
    $extract = $extractor->extract($archivePath, $destPath);
    if ($extract) {
      echo $GLOBALS['status']['success'];
    }
    else {
      echo $GLOBALS['status']['error'];
    }
    $zip_file_explode = explode('.', $file->filename);
    $zip_filename = $zip_file_explode[0];
    $zip_file_path = $path_img . '/' . $zip_filename;
    chmod($zip_file_path, 0777);
    $args = array(
      'path'      => $zip_file_path,
      'img_dir'   => $zip_file_path,
      'thumb_dir' => $path_thumb,
      'height'    => $width,
      'width'     => $height,
    );
    $img = new ImageResizer($args);
    $img->create();
    bulk_image_resizer_delete_directories($zip_file_path);
  }
  else {
    $args = array(
      'path'      => $path_img,
      'img_dir'   => $path_img,
      'thumb_dir' => $path_thumb,
      'height'    => $width,
      'width'     => $height,
    );
    $img = new ImageResizer($args);
    $img->create();
    bulk_image_resizer_delete_all_files($path_img);
  }
  drupal_set_message(t('Images stored on :- ' . $path_thumb));
}

/**
 * Implements custom hook for deleting files.
 */
function bulk_image_resizer_delete_all_files($str) {
  if (is_file($str)) {
    return unlink($str);
  }
  elseif (is_dir($str)) {
    $scan = glob(rtrim($str, '/') . '/*');
    foreach ($scan as $index => $path) {
      unlink($path);
    }
  }
}

/**
 * Implements custom hook for deleting directories.
 */
function bulk_image_resizer_delete_directories($target) {
  if (is_dir($target)) {
    $files = glob($target . '*', GLOB_MARK);
    foreach ($files as $file) {
      delete_files($file);
    }
    rmdir($target);
  }
  elseif (is_file($target)) {
    unlink($target);
  }
}
